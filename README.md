## Synopsis

This was made because kix desperately needs to stop creating bases every day.

## Motivation

Kix makes way to many bases.

## Installation

Download MCP910, Install like a normal client source

## Tests

Use sout

## Contributors

Kix, STG, badvibes, Whitefox, N3xuz, Red

## License

MIT liscense for freeware

## To Do

Nametags, Clickgui, 2d ESP, Shader ESP, Outline ESP, Main menu, Menu gui, Source-Styled console, freecam, phase, criticals, NoSlow, Jesus/Basilisk, AutoArmor, Silent BowAimbot, Strafe, Fastladder, flight, Trajectories, InventoryMove, Inventory+, Nuker, Speedy Gonzales, Nofall, Fastplace, Autofarm, Lenny command, TableFlip command, Mode System, Cleaner commands, option to use either supers or annotations in modules / commands.