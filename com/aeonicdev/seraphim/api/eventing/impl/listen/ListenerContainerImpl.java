package com.aeonicdev.seraphim.api.eventing.impl.listen;

import com.aeonicdev.seraphim.api.eventing.filter.Filter;
import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import com.aeonicdev.seraphim.api.eventing.listen.ListenerContainer;
import org.apache.commons.lang3.Validate;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sc4re
 */
public class ListenerContainerImpl implements ListenerContainer {

    protected final Object obj;
    protected final Method method;
    protected final Handler handler;
    protected final int priority;
    protected final Filter[] filters;
    protected final Map<Class<? extends Annotation>, Annotation> annotations = new HashMap<>();

    public ListenerContainerImpl(Object obj, Method m) {
        Validate.notNull(obj);
        Validate.notNull(m);
        this.obj = obj;
        this.method = m;
        this.handler = m.getAnnotation(Handler.class);
        this.filters = handler.filters();
        this.priority = handler.priority();
        for (Annotation a : m.getDeclaredAnnotations()) {
            if (a instanceof Handler)
                continue;
            annotations.put(a.getClass(), a);
        }
    }

    @Override
    public Object getObject() {
        return obj;
    }

    @Override
    public Method getMethod() {
        return method;
    }

    @Override
    public Handler getHandlerAnnotation() {
        return handler;
    }

    @Override
    public Filter[] getFilters() {
        return filters;
    }

    @Override
    public <T extends Annotation> T getAnnotation(Class<T> anno) {
       return (T)this.annotations.get(anno);
    }

    @Override
    public int hashCode() {
        return getObject().hashCode() ^ getMethod().hashCode();
    }
}
