package com.aeonicdev.seraphim.api.eventing.impl.filter;

import com.aeonicdev.seraphim.api.eventing.filter.FilterCache;
import com.aeonicdev.seraphim.api.eventing.filter.IEventFilter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sc4re
 */
public class FilterCacheImpl implements FilterCache {

    protected final Map<Class<? extends IEventFilter>, IEventFilter<?>> filters = new HashMap<>();

    @Override
    public IEventFilter<?>[] getFilters() {
        return filters.values().toArray(new IEventFilter[filters.size()]);
    }

    @Override
    public <T extends IEventFilter> void addFilter(T object) {
        filters.put(object.getClass().asSubclass(IEventFilter.class), object);
    }

    @Override
    public <T extends IEventFilter<?>> T getFilter(Class<T> filterClass) {
        return (T)filters.get(filterClass);
    }
}
