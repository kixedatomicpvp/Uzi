package com.aeonicdev.seraphim.api.eventing.impl.dispatch;

import com.aeonicdev.seraphim.api.eventing.dispatch.EventSender;
import com.aeonicdev.seraphim.api.eventing.filter.Filter;
import com.aeonicdev.seraphim.api.eventing.filter.FilterCache;
import com.aeonicdev.seraphim.api.eventing.filter.IEventFilter;
import com.aeonicdev.seraphim.api.eventing.impl.filter.FilterCacheImpl;
import com.aeonicdev.seraphim.api.eventing.listen.ListenerContainer;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * An implementation of an event sender.
 * @author sc4re
 */
public class EventSenderImpl implements EventSender {

    protected final List<ListenerContainer> listeners = new ArrayList<>();
    protected final FilterCache fcache = new FilterCacheImpl();

    public EventSenderImpl() {}

    @Override
    public synchronized void addListener(ListenerContainer listener) {
        Validate.notNull(listener);
        listeners.add(listener);
        Collections.sort(listeners, new ListenerContainerComparator());
    }

    @Override
    public synchronized void removeListener(Object obj) {
        Iterator<ListenerContainer> iter = listeners.iterator();
        int size = listeners.size();
        while (iter.hasNext()) {
            if (iter.next().getObject().equals(obj))
                iter.remove();
        }
        if (listeners.size() != size)
            Collections.sort(listeners, new ListenerContainerComparator());
    }

    @Override
    public FilterCache getFilterCache() {
        return fcache;
    }

    @Override
    @SuppressWarnings("unchecked") // yolo lol
    public synchronized <T> void send(T event) {
        if (listeners.size() == 0)
            return; // this does a lot for performance
        Iterator<ListenerContainer> iter = listeners.iterator();
        while (iter.hasNext()) {
            ListenerContainer lc = iter.next();
            Filter[] flr = lc.getFilters();
            boolean fail = false;
            for (Filter f : flr) {
                IEventFilter<T> fl = fcache.getFilter(f.type());
                if (fl == null) {
                    try {
                        fl = f.type().newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                        fail = true;
                        break;
                    }
                    fcache.addFilter(fl);
                }
                if (!fl.shouldSend(event, lc)) {
                    fail = true;
                    break;
                }
            }
            if (fail)
                continue;
            try {
                lc.getMethod().invoke(lc.getObject(), event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
