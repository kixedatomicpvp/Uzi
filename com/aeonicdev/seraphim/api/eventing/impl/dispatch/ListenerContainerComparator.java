package com.aeonicdev.seraphim.api.eventing.impl.dispatch;

import com.aeonicdev.seraphim.api.eventing.listen.ListenerContainer;

import java.util.Comparator;

/**
 * @author sc4re
 */
public class ListenerContainerComparator implements Comparator<ListenerContainer> {

    @Override
    public int compare(ListenerContainer o1, ListenerContainer o2) {
        return o1.getHandlerAnnotation().priority() - o2.getHandlerAnnotation().priority();
    }
}
