package com.aeonicdev.seraphim.api.eventing.impl;

import com.aeonicdev.seraphim.api.eventing.Event;
import com.aeonicdev.seraphim.api.eventing.EventManager;
import com.aeonicdev.seraphim.api.eventing.dispatch.EventSender;
import com.aeonicdev.seraphim.api.eventing.impl.dispatch.EventSenderImpl;
import com.aeonicdev.seraphim.api.eventing.impl.listen.ListenerContainerImpl;
import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import com.aeonicdev.seraphim.api.eventing.listen.ListenerContainer;
import org.apache.commons.lang3.Validate;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author sc4re
 */
public class EventManagerImpl implements EventManager {

    protected final Map<Class<? extends Event>, EventSender> senders = new ConcurrentHashMap<>();

    @Override
    public EventSender[] getSenders() {
        return senders.values().toArray(new EventSender[senders.size()]);
    }

    @Override
    public synchronized void registerListener(final Object obj) {
        Validate.notNull(obj);
        for (Method m : obj.getClass().getDeclaredMethods()) {
            Handler anno = m.getAnnotation(Handler.class);
            if (anno == null)
                continue;
            if (m.getParameterTypes().length != 1)
                continue;
            if (!Event.class.isAssignableFrom(m.getParameterTypes()[0]))
                continue;
            m.setAccessible(true);
            ListenerContainer lc = new ListenerContainerImpl(obj, m);
            Class<? extends Event> evClass = m.getParameterTypes()[0].asSubclass(Event.class);
            if (senders.get(evClass) == null) {
                EventSender es = new EventSenderImpl();
                es.addListener(lc);
                senders.put(evClass, es);
            } else {
                senders.get(evClass).addListener(lc);
            }
        }
    }

    @Override
    public synchronized void unregisterListener(final Object obj) {
        Validate.notNull(obj);
        for (Method m : obj.getClass().getDeclaredMethods()) {
            boolean access = m.isAccessible();
            m.setAccessible(true);
            Handler anno = m.getAnnotation(Handler.class);
            if (anno == null)
                continue;
            if (m.getParameterTypes().length != 1)
                continue;
            if (!Event.class.isAssignableFrom(m.getParameterTypes()[0]))
                continue;
            Class<? extends Event> evClass = m.getParameterTypes()[0].asSubclass(Event.class);
            if (senders.get(evClass) != null) {
                senders.get(evClass).removeListener(obj);
            }
        }
    }

    @Override
    public <K extends Event> EventSender getSender(Class<K> klass) {
        return senders.get(klass);
    }

    @Override
    public void call(Event event) {
        Class<? extends Event> evClass = event.getClass().asSubclass(Event.class);
        if (senders.get(evClass) == null)
            return;
        senders.get(evClass).send(event);
    }
}
