package com.aeonicdev.seraphim.api.eventing.listen;

import com.aeonicdev.seraphim.api.eventing.filter.Filter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Contains any and all data necessary for sending events.
 * @author sc4re
 */
public interface ListenerContainer {
    /**
     * Returns a strong reference to the object in question.
     * @return The object.
     */
    public Object getObject();

    /**
     * Returns the method that must be invoked upon the object provided.
     * @return The method.
     */
    public Method getMethod();

    /**
     * Returns the handler annotations that is on the method.
     * @return The handler annotations.
     */
    public Handler getHandlerAnnotation();

    /**
     * The filters that are to be checked with before the listener is invoked.
     * @return The filters.
     */
    public Filter[] getFilters();

    /**
     * Returns any other annotations that might be on the method
     * Note: Cache all annotations at startup to
     * @param anno The annota
     * @param <T>
     * @return
     */
    public <T extends Annotation> T getAnnotation(Class<T> anno);
}
