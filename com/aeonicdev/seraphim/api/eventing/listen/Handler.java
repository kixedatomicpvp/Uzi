package com.aeonicdev.seraphim.api.eventing.listen;

import com.aeonicdev.seraphim.api.eventing.filter.Filter;

import java.lang.annotation.*;

/**
 * Defines the interface for an arbitrary message instance.
 * @author sc4re
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Inherited
@Target(value = {ElementType.METHOD})
public @interface Handler {
    /**
     * The priority of this handler. Lower numbers will be called later and later on the
     * @return The priority of this handler.
     */
    int priority() default 0;

    /**
     * The filters this handler uses to filter events.
     * @return The filters.
     */
    Filter[] filters() default {};
}
