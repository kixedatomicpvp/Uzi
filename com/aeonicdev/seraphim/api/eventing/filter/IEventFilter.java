package com.aeonicdev.seraphim.api.eventing.filter;

import com.aeonicdev.seraphim.api.eventing.listen.ListenerContainer;

/**
 * Validates whether an event
 * @author sc4re
 */

public interface IEventFilter<T> {
    boolean shouldSend(T event, ListenerContainer container);
}
