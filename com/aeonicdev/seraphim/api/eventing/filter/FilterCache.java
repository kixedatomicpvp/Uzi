package com.aeonicdev.seraphim.api.eventing.filter;

/**
 * This interface is used to hold
 * @author sc4re
 */
public interface FilterCache {
    /**
     * Returns the currently cached filters.
     * @return
     */
    public IEventFilter<?>[] getFilters();

    /**
     * Adds a filter to the filter cache.
     * @param object The event filter to add.
     * @param <T> The generic type of the event filter.
     */
    public <T extends IEventFilter> void addFilter(T object);

    /**
     * Returns a filter by it's class.
     * @param filterClass
     * @param <T>
     * @return
     */
    public <T extends IEventFilter<?>> T getFilter(Class<T> filterClass);

}
