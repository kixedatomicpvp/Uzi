package com.aeonicdev.seraphim.api.eventing.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the interface for
 * @author sc4re
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.ANNOTATION_TYPE})
public @interface Filter {
    /**
     * The class of the event filter to use.
     * @return The class
     */
    Class<? extends IEventFilter> type();
}
