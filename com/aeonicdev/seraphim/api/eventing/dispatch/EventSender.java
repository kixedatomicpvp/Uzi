package com.aeonicdev.seraphim.api.eventing.dispatch;

import com.aeonicdev.seraphim.api.eventing.filter.FilterCache;
import com.aeonicdev.seraphim.api.eventing.listen.ListenerContainer;

/**
 * @author sc4re
 */
public interface EventSender {
    /**
     * Adds a listener to the event sender.
     * @param listener The listener to add.
     */
    public void addListener(ListenerContainer listener);

    /**
     * Removes a listener by it's object.
     * @param obj The object to remove.
     */
    public void removeListener(Object obj);

    /**
     * The filter cache that caches all instantiated filters/
     * @return The filter cache.
     */
    public FilterCache getFilterCache();
    /**
     * Sends the event through all of the listeners bound to this sender.
     * @param event The event to send.
     */
    public <T> void send(T event);
}
