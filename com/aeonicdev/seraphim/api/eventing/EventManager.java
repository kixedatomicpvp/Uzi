package com.aeonicdev.seraphim.api.eventing;

import com.aeonicdev.seraphim.api.eventing.dispatch.EventSender;

/**
 * @author sc4re
 */
public interface EventManager {
    /**
     * Returns the event senders contained by this event manager.
     * @return The senders.
     */
    public EventSender[] getSenders();

    /**
     * Registers the listener with various event senders.
     * @param obj The object to unregister.
     */
    public void registerListener(Object obj);

    /**
     * Unregisters a listener from it's respective event senders.
     * @param obj The object to unregister.
     */
    public void unregisterListener(Object obj);

    /**
     * Returns the sender of a specific event type.
     * @param klass The class of the event to retrieve
     * @param <K> The generic type of the event to retrieve
     * @return The event sender of type K
     */
    public <K extends Event> EventSender getSender(Class<K> klass);

    /**
     * Sends the event to its appropriate event sender,
     * @param event
     */
    public void call(Event event);
}
