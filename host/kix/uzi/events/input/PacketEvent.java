package host.kix.uzi.events.input;

import com.aeonicdev.seraphim.api.eventing.Event;
import net.minecraft.network.Packet;

/**
 * Created by Kix on 5/5/2017.
 */
public class PacketEvent extends Event {

    private PacketType type;
    private Packet packet;
    private boolean cancelled;

    public PacketEvent(PacketType type, Packet packet) {
        this.type = type;
        this.packet = packet;
    }

    public void setType(PacketType type) {
        this.type = type;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Packet getPacket() {
        return packet;
    }

    public void setPacket(Packet packet) {
        this.packet = packet;
    }

    public PacketType getType() {
        return type;
    }

    public enum PacketType {
        SEND,
        RECEIVE
    }

}
