package host.kix.uzi.events.input;

import com.aeonicdev.seraphim.api.eventing.Event;

/**
 * Created by Kix on 5/5/2017.
 */
public class KeyboardEvent extends Event {

    /**
     * The default macro that could just so happen to be the macro of the module.
     */
    private int macro;

    public KeyboardEvent(int macro) {
        this.macro = macro;
    }

    public int getMacro() {
        return macro;
    }
}
