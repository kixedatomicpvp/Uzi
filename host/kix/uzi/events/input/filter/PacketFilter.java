package host.kix.uzi.events.input.filter;

import java.lang.reflect.ParameterizedType;

import com.aeonicdev.seraphim.api.eventing.filter.IEventFilter;
import com.aeonicdev.seraphim.api.eventing.listen.ListenerContainer;

import host.kix.uzi.events.input.PacketEvent;
import net.minecraft.network.Packet;

public final class PacketFilter implements IEventFilter<PacketEvent> {
	@Override
	public boolean shouldSend(PacketEvent event, ListenerContainer container) {
		if (!container.getMethod().isAnnotationPresent(AllowedPackets.class))
			return false;

		AllowedPackets packets = container.getMethod().getAnnotation(AllowedPackets.class);
		if (packets == null)
			return true;

		for (Class<? extends Packet> packet : packets.value())
			if (packet.equals(event.getPacket().getClass()))
				return true;

		return false;
	}
}