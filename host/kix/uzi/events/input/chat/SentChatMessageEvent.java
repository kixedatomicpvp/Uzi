package host.kix.uzi.events.input.chat;

import com.aeonicdev.seraphim.api.eventing.Event;
import host.kix.uzi.Uzi;
import host.kix.uzi.command.framework.Command;
import host.kix.uzi.utility.chat.Logger;

/**
 * Created by Kix on 5/6/2017.
 */
public class SentChatMessageEvent extends Event{

    private boolean cancelled;
    public String message;

    public SentChatMessageEvent(String message) {
        this.message = message;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public void checkCommands() {
        if (message.startsWith(".")) {
            for (final Command command : Uzi.getUzi().getCommandManager().getContents()) {
                if (message.split(" ")[0].equalsIgnoreCase("."
                        + command.getLabel())) {
                    try {
                        command.init(message);
                    } catch (final Exception e) {
                        Logger.logToChat("Invalid arguments!");
                    }
                    setCancelled(true);
                } else {
                    for (final String alias : command.getHandles()) {
                        if (message.split(" ")[0].equalsIgnoreCase("." + alias)) {
                            try {
                                command.init(message);
                            } catch (final Exception e) {
                                Logger.logToChat("Invalid arguments!");
                            }
                            setCancelled(true);
                        }
                    }
                }
            }

            if (!isCancelled()) {
                Logger.logToChat("Command \"" + message + "\" was not found!");
                setCancelled(true);
            }
        }
    }

}
