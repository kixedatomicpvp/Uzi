package host.kix.uzi.events.update;

import com.aeonicdev.seraphim.api.eventing.Event;
import net.minecraft.client.entity.EntityPlayerSP;

/**
 * Created by Kix on 5/5/2017.
 */
public class UpdateEvent extends Event {

    private EntityPlayerSP player;
    public boolean alwaysSend;
    public double y;
    public float yaw, pitch;
    public boolean onGround;
    public UpdateType type;
    public boolean cancelled;

    public UpdateEvent(double y, float[] rot, boolean onGround, EntityPlayerSP player) {
        this.y = y;
        this.player = player;
        this.yaw = rot[0];
        this.pitch = rot[1];
        this.onGround = onGround;
        this.type = UpdateType.PRE;
    }

    public UpdateEvent() {
        this.type = UpdateType.POST;
    }


    public EntityPlayerSP getPlayer() {
        return player;
    }

    public void setPlayer(EntityPlayerSP player) {
        this.player = player;
    }

    public boolean isAlwaysSend() {
        return alwaysSend;
    }

    public void setAlwaysSend(boolean alwaysSend) {
        this.alwaysSend = alwaysSend;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public boolean isOnGround() {
        return onGround;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    public UpdateType getType() {
        return type;
    }

    public void setType(UpdateType type) {
        this.type = type;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public enum UpdateType {
        PRE,
        POST
    }


}
