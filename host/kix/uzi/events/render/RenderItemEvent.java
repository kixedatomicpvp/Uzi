package host.kix.uzi.events.render;

import com.aeonicdev.seraphim.api.eventing.Event;
import net.minecraft.entity.item.EntityItem;

/**
 * Created by Kix on 5/6/2017.
 */
public class RenderItemEvent extends Event {

    private boolean cancelled;
    private final State state;
    private EntityItem item;

    public RenderItemEvent(State state, EntityItem item) {
        this.state = state;
        this.item = item;
    }

    public State getState() {
        return state;
    }

    public EntityItem getItem() {
        return item;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public enum State {
        PRE,
        POST
    }

}
