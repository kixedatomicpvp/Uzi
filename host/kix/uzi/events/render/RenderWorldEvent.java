package host.kix.uzi.events.render;

import com.aeonicdev.seraphim.api.eventing.Event;

/**
 * Created by Kix on 5/5/2017.
 */
public class RenderWorldEvent extends Event {

    private float partialTicks;

    public RenderWorldEvent(float partialTicks) {
        this.partialTicks = partialTicks;
    }

    public float getPartialTicks() {
        return partialTicks;
    }

    public void setPartialTicks(float partialTicks) {
        this.partialTicks = partialTicks;
    }
}
