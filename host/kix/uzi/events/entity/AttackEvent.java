package host.kix.uzi.events.entity;

import com.aeonicdev.seraphim.api.eventing.Event;
import net.minecraft.entity.Entity;

/**
 * Created by Kix on 5/13/2017.
 */
public class AttackEvent extends Event {

    private Entity entity;

    public AttackEvent(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }
}
