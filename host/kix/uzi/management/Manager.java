package host.kix.uzi.management;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Kix on 5/5/2017.
 * This class should be fairly self explanatory which is why I am not documenting it
 */
public class Manager<T> {

    private List<T> contents = new CopyOnWriteArrayList<>();

    public List<T> getContents() {
        return contents;
    }
}
