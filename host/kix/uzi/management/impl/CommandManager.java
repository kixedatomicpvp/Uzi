package host.kix.uzi.management.impl;

import host.kix.uzi.Uzi;
import host.kix.uzi.command.framework.Command;
import host.kix.uzi.command.impl.*;
import host.kix.uzi.management.Manager;
import host.kix.uzi.utility.chat.Logger;

/**
 * Created by Kix on 5/6/2017.
 */
public class CommandManager extends Manager<Command> {

    public CommandManager() {
        getContents().add(new HelpCommand());
        getContents().add(new ToggleCommand());
        getContents().add(new BindCommand());
        getContents().add(new LennyCommand());
        getContents().add(new TableFlipCommand());
        getContents().add(new GrabIPCommand());
        getContents().add(new FindCommand());
        createDefaultSettingsCommands();
    }

    private void createDefaultSettingsCommands() {
        Uzi.getUzi().getModuleManager().getContents().forEach(module -> {
            if (!module.getValues().isEmpty()) {
                getContents().add(new Command(module.getLabel(), "Changes settings for " + module.getLabel(), module.getLabel()) {
                    @Override
                    public void init(String message) {
                        String[] args = message.split(" ");

                        module.getValues().forEach(value -> {
                            if (args[1].equalsIgnoreCase(value.getName())) {
                                if (value.getValue() instanceof Boolean) {
                                    value.setValue(!(boolean) value.getValue());
                                    Logger.logToChat(value.getName() + " has been toggled for " + module.getLabel());
                                }
                                if (value.getValue() instanceof Integer) {
                                    value.setValue(Integer.parseInt(args[2]));
                                    Logger.logToChat(value.getName() + " has been set to " + value.getValue() + "for " + module.getLabel());
                                }
                                if (value.getValue() instanceof Double) {
                                    value.setValue(Double.parseDouble(args[2]));
                                    Logger.logToChat(value.getName() + " has been set to " + value.getValue() + "for " + module.getLabel());
                                }
                                if (value.getValue() instanceof Float) {
                                    value.setValue(Float.parseFloat(args[2]));
                                    Logger.logToChat(value.getName() + " has been set to " + value.getValue() + "for " + module.getLabel());
                                }
                            }
                        });
                    }
                });
            }
        });
    }

}
