package host.kix.uzi.management.impl;

import host.kix.uzi.management.Manager;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.impl.combat.*;
import host.kix.uzi.module.impl.misc.*;
import host.kix.uzi.module.impl.movement.Flight;
import host.kix.uzi.module.impl.movement.Glide;
import host.kix.uzi.module.impl.movement.Speed;
import host.kix.uzi.module.impl.movement.Sprint;
import host.kix.uzi.module.impl.render.*;
import host.kix.uzi.module.impl.world.AutoExtinguish;
import host.kix.uzi.module.impl.world.AutoRespawn;
import host.kix.uzi.module.impl.world.FastPlace;

/**
 * Created by Kix on 5/5/2017.
 */
public class ModuleManager extends Manager<Module> {

    public ModuleManager() {
        getContents().add(new Overlay());
        getContents().add(new Sprint());
        getContents().add(new Esp());
        getContents().add(new AntiWeather());
        getContents().add(new Retard());
        getContents().add(new Speed());
        getContents().add(new AutoHeal());
        getContents().add(new AntiTabComplete());
        getContents().add(new NoItems());
        getContents().add(new KillAura());
        getContents().add(new Knockback());
        getContents().add(new NoRotate());
        getContents().add(new FakeLag());
        getContents().add(new AutoRespawn());
        getContents().add(new FastPlace());
        getContents().add(new Brightness());
        getContents().add(new Flight());
        getContents().add(new Glide());
        getContents().add(new AutoSword());
        getContents().add(new SkinFlash());
        getContents().add(new AntiAim());
        getContents().add(new AutoExtinguish());
    }
    public Module find(String label) {
        for (Module mod : this.getContents()) {
            if (mod.getLabel().equalsIgnoreCase(label)) {
                return mod;
            }
        }
        return null;
    }


}
