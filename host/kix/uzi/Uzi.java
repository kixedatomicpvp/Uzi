package host.kix.uzi;

import com.aeonicdev.seraphim.api.eventing.impl.EventManagerImpl;
import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.input.KeyboardEvent;
import host.kix.uzi.gui.alt.AltManager;
import host.kix.uzi.management.impl.CommandManager;
import host.kix.uzi.management.impl.ModuleManager;
import host.kix.uzi.module.impl.render.Overlay;
import net.minecraft.client.Minecraft;

/**
 * Created by Kix on 5/2/2017.
 */
public final class Uzi {

    /**
     * Variable which holds the instance of this class that the whole client will use!
     * final because never edited and constant
     * static to access across classes.
     */
    private static final Uzi uzi = new Uzi();
    /**
     * Variable which holds the instance of the event system used virtually around the client.
     */
    private final EventManagerImpl eventManager = new EventManagerImpl();

    /**
     * The module manager which holds all of the modules and manages them
     */
    private ModuleManager moduleManager;

    /**
     * The command Manager which holds all of the commands and handles them
     */
    private CommandManager commandManager;

    /**
     * The Alt Manager which holds the alts
     */
    private AltManager altManager = new AltManager();

    /**
     * Initialization void in which will be called in the Minecraft class and all of the areas where I feel necessary if the Client needs to reinitialize.
     */
    public void initialization() {
        Minecraft.getMinecraft().setDefaultTitle(String.format("%s | %s b%s", Minecraft.getMinecraft().getDefaultTitle(), getLabel(), getBuild()));
        moduleManager = new ModuleManager();
        commandManager = new CommandManager();
        eventManager.registerListener(this);
    }

    /**
     * Handles the keyboard event
     */
    @Handler
    public void keyParsing(KeyboardEvent event) {
        moduleManager.getContents().forEach(module -> {
            if (event.getMacro() == module.getMacro()) {
                module.toggle();
            }
        });
    }

    /**
     * @return command manager
     */
    public CommandManager getCommandManager() {
        return commandManager;
    }

    /**
     * @return alt manager
     */
    public AltManager getAltManager() {
        return altManager;
    }

    /**
     * @return moduleManager
     */
    public ModuleManager getModuleManager() {
        return moduleManager;
    }

    /**
     * @return the client name which I decided not to make its own variable ;).
     */
    public String getLabel() {
        return "Uzi";
    }

    /**
     * @return the version which I believe is 6. I hope to god im not wrong :/.
     */
    public int getBuild() {
        return 6;
    }

    /**
     * @return the eventmanager
     */
    public EventManagerImpl getEventManager() {
        return eventManager;
    }

    /**
     * @return this class with my extra special Uzi instance
     */
    public static Uzi getUzi() {
        return uzi;
    }
}
