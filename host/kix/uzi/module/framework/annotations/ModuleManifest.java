package host.kix.uzi.module.framework.annotations;

import host.kix.uzi.module.framework.Category;

import java.awt.*;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Kix on 5/2/2017.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ModuleManifest {

    /**
     * Allows me to create the label through a annotations instead of a gay super / constructor combo
     *
     * @return the label of the Module
     */
    String label();

    /**
     * Allows me to set a default macro for the module. This may never be used but is here as a quick precaution
     *
     * @return the macro of the Module
     */
    int macro() default 0;

    /**
     * Sets the category of the module
     */
    Category category();

    /**
     * The color of the module in the hud
     */
    int color() default 0xFFFFFF;

}
