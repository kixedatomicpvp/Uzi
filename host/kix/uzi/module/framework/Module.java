package host.kix.uzi.module.framework;

import host.kix.uzi.Uzi;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.module.value.Value;
import host.kix.uzi.utility.ModuleHelper;

import java.util.*;

/**
 * Created by Kix on 5/5/2017.
 */
public class Module extends ModuleHelper {

    /**
     * Labels the Module
     */
    private String label;

    /**
     * Macro which can induce the toggling of the module
     */
    private int macro;

    /**
     * Categorizes the Module
     */
    private Category category;

    /**
     * Is the module currently running?
     */
    private boolean running;

    /**
     * The Color of the module
     */
    private int color;

    /**
     * Holds a list of the values
     */
    private List<Value> values = new ArrayList();

    /**
     * Holds a list of the modes
     */
    private List<ModuleMode> modes;

    /**
     * Module mode
     */
    private ModuleMode mode;

    /**
     * Label with a suffix
     */
    private String labelWithSuffix;

    /**
     * Constructor which will make sure all of the annotations are used properly.
     */
    public Module() {

        ModuleManifest manifesto = this.getClass().getAnnotation(ModuleManifest.class);

        // Grabs the label from the Annotation "ModuleManifest"
        this.label = manifesto.label();
        // Grabs the macro from the Annotation "ModuleManifest"
        this.macro = manifesto.macro();
        // Grabs the category from the Annotation "ModuleManifest"
        this.category = manifesto.category();
        // Grabs the color from the Annotation "ModuleManifest"
        this.color = manifesto.color();
        // Makes the modes list an arraylist
        modes = new ArrayList<>();
    }


    /**
     * @return the values
     */
    public List<Value> getValues() {
        return values;
    }

    public List<ModuleMode> getModes() {
        return modes;
    }

    /**
     * The Mode
     *
     * @return
     */
    public ModuleMode getMode() {
        return mode;
    }

    /**
     * Adds a mode
     */
    public void addMode(ModuleMode mode) {
        modes.add(mode);
        if (this.mode == null) {
            this.mode = mode;
        }
    }

    /**
     * Sets the macro
     *
     * @param macro
     */
    public void setMacro(int macro) {
        this.macro = macro;
    }

    /**
     * Sets the label with suffix
     *
     * @param labelWithSuffix
     */
    public void setLabelWithSuffix(String labelWithSuffix) {
        this.labelWithSuffix = labelWithSuffix;
    }

    /**
     * @return the labelWithSuffix
     */
    public String getLabelWithSuffix() {
        return labelWithSuffix;
    }

    /**
     * Sets the mode
     */
    public void setMode(ModuleMode moduleMode) {
        if (this.mode != null) {
            Uzi.getUzi().getEventManager().unregisterListener(mode);
            mode.onDisable();
        }
        if (isRunning()) {
            Uzi.getUzi().getEventManager().registerListener(moduleMode);
            moduleMode.onEnable();
        }
        this.mode = moduleMode;
    }

    /**
     * @return the color
     */
    public int getColor() {
        return color;
    }

    /**
     * @return the category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @return the macro
     */
    public int getMacro() {
        return macro;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Method which does certain stuff on the enabling of the module
     */
    public void onEnable() {
        // Registers the child classes as listeners for the event system
        Uzi.getUzi().getEventManager().registerListener(this);
    }

    /**
     * Method which does certain stuff on the disabling of the module
     */
    public void onDisable() {
        // Unregisters the child classes as listeners fro the event system
        Uzi.getUzi().getEventManager().unregisterListener(this);
    }

    /**
     * @return if the module is running or not
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * @param running which sets the module to running either false or true
     */
    public void setRunning(boolean running) {
        this.running = running;
        if (running) {
            if (this.mode != null) {
                Uzi.getUzi().getEventManager().registerListener(this.mode);
                this.mode.onEnable();
            }
            onEnable();
        } else {
            if (this.mode != null) {
                Uzi.getUzi().getEventManager().unregisterListener(mode);
                mode.onDisable();
            }
            onDisable();
        }
    }

    /**
     * Switches the module state to the opposite which allows the effect of "toggling"
     */
    public void toggle() {
        setRunning(!isRunning());
    }

}
