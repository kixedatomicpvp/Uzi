package host.kix.uzi.module.framework;

import org.apache.commons.lang3.text.WordUtils;

/**
 * Created by Kix on 5/5/2017.
 */
public enum Category {

    COMBAT,
    MOVEMENT,
    MISC,
    RENDER,
    WORLD;

    /**
     * Grabs the category's name and formats it properly.
     * @return the properly formatted version of the category using the WorldUtils class provided by the apache library.
     */
    public String formatProperly() {
        return WordUtils.capitalizeFully(super.toString());
    }
}
