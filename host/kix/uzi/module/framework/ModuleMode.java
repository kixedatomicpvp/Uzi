package host.kix.uzi.module.framework;

import net.minecraft.client.Minecraft;

/**
 * Created by Kix on 5/7/2017.
 */
public class ModuleMode<T> {

    protected T parent;
    private String name;
    protected Minecraft minecraft = Minecraft.getMinecraft();

    public ModuleMode(T parent, String name) {
        this.parent = parent;
        this.name = name;
    }

    public void onEnable() {
    }

    public void onDisable() {
    }

    public T getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }


}
