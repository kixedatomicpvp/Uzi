package host.kix.uzi.module.impl.render;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.render.RenderItemEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Kix on 5/6/2017.
 */
@ModuleManifest(label = "NoItems", macro = Keyboard.KEY_N, category = Category.RENDER, color = 0x2FDE8F)
public class NoItems extends Module {

    public NoItems() {
        setLabelWithSuffix("No Items");
    }

    @Handler
    public void renderItem(RenderItemEvent event) {
        event.setCancelled(true);
        minecraft.theWorld.removeEntity(event.getItem());
    }

}
