package host.kix.uzi.module.impl.render;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

/**
 * Created by Kix on 5/13/2017.
 */
@ModuleManifest(label = "Brightness", category = Category.RENDER, color = 0xF5EC73)
public class Brightness extends Module {

    @Handler
    public void updateGameLoop(UpdateEvent event) {
        minecraft.thePlayer.addPotionEffect(new PotionEffect(Potion.nightVision.getId(), 1000000));
    }

    @Override
    public void onDisable() {
        super.onDisable();
        minecraft.thePlayer.removePotionEffect(Potion.nightVision.getId());
    }
}
