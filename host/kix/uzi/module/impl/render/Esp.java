package host.kix.uzi.module.impl.render;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.render.RenderWorldEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.module.value.Value;
import host.kix.uzi.utility.RenderUtility;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Kix on 5/5/2017.
 */
@ModuleManifest(label = "Esp", macro = Keyboard.KEY_B, category = Category.RENDER, color = 0xB9DE85)
public class Esp extends Module {

    private Value<Boolean> boxes = new Value<Boolean>("Boxes", true);
    private Value<Boolean> spines = new Value<Boolean>("Spines", true);

    public Esp() {
        getValues().add(boxes);
        getValues().add(spines);
    }

    @Handler
    public void render(RenderWorldEvent event) {
        if (!minecraft.isGuiEnabled())
            return;
        RenderUtility.beginGl();
        for (Object obj : minecraft.theWorld.playerEntities) {
            EntityPlayer entity = (EntityPlayer) obj;
            if (!isValid(entity))
                continue;

            float partialTicks = event.getPartialTicks();
            double x = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * partialTicks - minecraft.getRenderManager().renderPosX;
            double y = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * partialTicks - minecraft.getRenderManager().renderPosY;
            double z = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * partialTicks - minecraft.getRenderManager().renderPosZ;

            if (boxes.getValue()) {
                GlStateManager.pushMatrix();
                GlStateManager.translate(x, y, z);
                GlStateManager.rotate(-entity.rotationYaw, 0.0F, entity.height, 0.0F);
                GlStateManager.translate(-x, -y, -z);
                drawBoxes(entity, x, y, z);
                GlStateManager.popMatrix();
            }

            if (spines.getValue()) {
                drawSpines(entity, x, y, z);
            }
        }
        RenderUtility.endGl();

    }

    private boolean isValid(EntityPlayer playerEntity) {
        return playerEntity != minecraft.thePlayer && !playerEntity.isDead;
    }


    private void drawBoxes(Entity entity, double x, double y, double z) {
        AxisAlignedBB box = AxisAlignedBB.fromBounds(x - entity.width, y, z - entity.width, x + entity.width, y + entity.height + 0.2D, z + entity.width);
        if (entity instanceof EntityLivingBase) {
            box = AxisAlignedBB.fromBounds(x - entity.width + 0.2D, y, z - entity.width + 0.2D, x + entity.width - 0.2D, y + entity.height + (entity.isSneaking() ? 0.02D : 0.2D), z + entity.width - 0.2D);
        }

        final float distance = minecraft.getThePlayer().getDistanceToEntity(entity);
        float[] color = new float[]{0.0F, 0.9F, 0.0F};
        if (((EntityLivingBase) entity).hurtTime > 2) {
            color = new float[]{0.9F, 0.0F, 0.0F};
        }
        GlStateManager.color(color[0], color[1], color[2], 0.6F);
        RenderUtility.drawLines(box);
        GlStateManager.color(color[0], color[1], color[2], 0.6F);
        RenderGlobal.drawOutlinedBoundingBox(box, -1);

    }

    private void drawSpines(Entity entity, double x, double y, double z) {
        final float distance = minecraft.getThePlayer().getDistanceToEntity(entity);
        float[] color = new float[]{0.0F, 0.90F, 0.0F};
        if (((EntityLivingBase) entity).hurtTime > 2) {
            color = new float[]{0.9F, 0.0F, 0.0F};
        }
        GlStateManager.color(color[0], color[1], color[2], 1.0F);
        Tessellator var2 = Tessellator.getInstance();
        WorldRenderer var3 = var2.getWorldRenderer();
        var3.startDrawing(2);
        var3.addVertex(x, y, z);
        var3.addVertex(x, y + entity.getEyeHeight(), z);
        var2.draw();
    }

}
