package host.kix.uzi.module.impl.render;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.Uzi;
import host.kix.uzi.events.input.KeyboardEvent;
import host.kix.uzi.events.render.RenderGameOverlayEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.module.value.Value;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by Kix on 5/5/2017.
 */

@ModuleManifest(label = "Overlay", macro = Keyboard.KEY_H, category = Category.RENDER, color = 0xDEDEDE)
public class Overlay extends Module {

    private float hue;
    private Value<Boolean> watermark = new Value<Boolean>("Watermark", true);
    private Value<Boolean> moduleList = new Value<Boolean>("Arraylist", true);
    private Value<Boolean> rainbow = new Value<Boolean>("Rainbow", false);
    private static Value<Boolean> tabUi = new Value<Boolean>("TabGui", true);
    private int uziColor;


    /**
     * Adds the values
     */
    public Overlay() {
        getValues().add(watermark);
        getValues().add(moduleList);
        getValues().add(rainbow);
        hue = 0;
    }

    /**
     * Renders all of the needed methods inside of the RenderGameOverlayEvent
     */
    @Handler
    public void render(RenderGameOverlayEvent event) {
        if (watermark.getValue())
            renderWatermark();
        if (moduleList.getValue())
            renderModuleList();
    }

    /**
     * Renders the watermark onto the screen
     */
    private void renderWatermark() {
        if (rainbow.getValue()) {
            GlStateManager.pushMatrix();
            GlStateManager.scale(1.5F, 1.5F, 1.5F);
            minecraft.getFontRendererObj().drawStringWithShadow("U", 2, 2, uziColor);
            GlStateManager.popMatrix();
            minecraft.getFontRendererObj().drawStringWithShadow("zi", 13, 7, Color.white.getRGB());
            GlStateManager.pushMatrix();
            GlStateManager.scale(0.8F, 0.8F, 0.8F);
            minecraft.getFontRendererObj().drawStringWithShadow("b" + Uzi.getUzi().getBuild(), 15, 2, Color.lightGray.getRGB());
            GlStateManager.popMatrix();
        } else {
            minecraft.fontRendererObj.drawStringWithShadow(Uzi.getUzi().getLabel(), 2, 2, Color.white.getRGB());
        }
    }

    /**
     * Renders the module list onto the screen
     */
    private void renderModuleList() {
        ScaledResolution sr = new ScaledResolution(this.minecraft, this.minecraft.displayWidth, this.minecraft.displayHeight);
        int yCount = 2;
        this.hue += 0.9;
        List<Module> mods = new ArrayList<Module>();
        Uzi.getUzi().getModuleManager().getContents().forEach(module -> {
            mods.add(module);
        });

        Collections.sort(mods, new ModuleComparator());

        float h = this.hue;

        for (Module module : mods) {
            if (module.isRunning()) {
                if (h > 50f) {
                    h = 0.0f;
                }
                int width = minecraft.fontRendererObj.getStringWidth(module.getLabel()) + 3;
                final Color color = new Color(Color.HSBtoRGB((float) (minecraft.getThePlayer().ticksExisted / 50.0 + Math.sin(h / 50.0 * 1.5707963267948966)) % 1.0f, 0.5882353f, 1.0f));
                final int c = color.getRGB();
                h += 1.5;
                uziColor = c;
                minecraft.fontRendererObj.drawStringWithShadow(module.getLabel(), sr.getScaledWidth() - minecraft.fontRendererObj.getStringWidth(module.getLabel()) - 2, yCount, rainbow.getValue() ? c : module.getColor());
                yCount += minecraft.fontRendererObj.FONT_HEIGHT;
            }
        }

    }

    public static Value<Boolean> getTabUi() {
        return tabUi;
    }

    public class ModuleComparator implements Comparator<Module> {

        @Override
        public int compare(Module o1, Module o2) {
            if (minecraft.getFontRendererObj().getStringWidth(o1.getLabel()) < minecraft.getFontRendererObj().getStringWidth(o2.getLabel())) {
                return 1;
            } else if (minecraft.getFontRendererObj().getStringWidth(o1.getLabel()) > minecraft.getFontRendererObj().getStringWidth(o2.getLabel())) {
                return -1;
            } else {
                return 0;
            }
        }

    }


}
