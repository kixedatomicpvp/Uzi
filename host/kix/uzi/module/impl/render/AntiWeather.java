package host.kix.uzi.module.impl.render;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.TickEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Kix on 5/5/2017.
 */
@ModuleManifest(label = "AntiWeather", macro = Keyboard.KEY_L, category = Category.RENDER, color = 0x94DEDB)
public class AntiWeather extends Module {

    public AntiWeather() {
        setLabelWithSuffix("Anti Weather");
    }

    @Handler
    public void tick(TickEvent event) {
        if (minecraft.theWorld.isRaining()) {
            minecraft.theWorld.setRainStrength(0F);
            minecraft.theWorld.setThunderStrength(0F);
        }
    }

}
