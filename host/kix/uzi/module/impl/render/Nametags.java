package host.kix.uzi.module.impl.render;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;

import host.kix.uzi.events.render.RenderWorldEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.utility.RenderUtility;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.EntityPlayer;

@ModuleManifest(label = "Nametags", macro = Keyboard.KEY_NONE, category = Category.RENDER, color = 0xB9DE85)
public class Nametags extends Module {
    
    private int getColour(EntityPlayer ep) {
    	return ep.getUniqueID().equals("2c3174fc0c6b4cfbbb2b0069bf7294d1") ? 0xFFFF7FD1 : ep.isSneaking() ? 0xFFFF4000 : 0xFFFFFFFF;
    }
}
