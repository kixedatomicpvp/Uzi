package host.kix.uzi.module.impl.combat;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.entity.AttackEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

/**
 * Created by Kix on 5/13/2017.
 */
@ModuleManifest(label = "AutoSword", category = Category.COMBAT, color = 0xEA73F5)
public class AutoSword extends Module {

    @Handler
    public void attackEntity(AttackEvent event){
        getBestWeapon(event.getEntity());
    }

    private void getBestWeapon(Entity e) {
        float damageModifier = 0;
        int newItem = -1;
        for (int slot = 0; slot < 9; slot++) {
            ItemStack stack = minecraft.thePlayer.inventory.mainInventory[slot];
            if (stack == null) {
                continue;
            }
            if (stack.getItem() instanceof ItemSword) {
                ItemSword is = (ItemSword) stack.getItem();
                float damage = is.func_150931_i() + (is.hasEffect(stack) ? getEnchantDamageVsEntity(stack, e) : 0);
                if (damage >= damageModifier) {
                    newItem = slot;
                    damageModifier = damage;
                }
            }
        }
        if (newItem > -1) {
            minecraft.thePlayer.inventory.currentItem = newItem;
        }
    }

    private int getEnchantDamageVsEntity(ItemStack i, Entity e) {
        if (e instanceof EntityZombie || e instanceof EntityPigZombie || e instanceof EntitySkeleton) {
            return EnchantmentHelper.getEnchantmentLevel(Enchantment.field_180314_l.effectId, i) + EnchantmentHelper.getEnchantmentLevel(Enchantment.field_180315_m.effectId, i);
        } else if (e instanceof EntitySpider) {
            return EnchantmentHelper.getEnchantmentLevel(Enchantment.field_180314_l.effectId, i) + EnchantmentHelper.getEnchantmentLevel(Enchantment.field_180312_n.effectId, i);
        } else {
            return EnchantmentHelper.getEnchantmentLevel(Enchantment.field_180314_l.effectId, i);
        }
    }

}
