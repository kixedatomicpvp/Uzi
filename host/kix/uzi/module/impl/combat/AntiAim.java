package host.kix.uzi.module.impl.combat;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.input.PacketEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;

/**
 * Created by Kix on 5/13/2017.
 */
@ModuleManifest(label = "AntiAim", category = Category.COMBAT, color = 0xF573BD)
public class AntiAim extends Module {

    @Handler
    public void packetListener(PacketEvent event) {
        if (this.isRunning()) {
            if ((event.getPacket() instanceof S08PacketPlayerPosLook)) {
                S08PacketPlayerPosLook packet = (S08PacketPlayerPosLook) event.getPacket();
                if ((AntiAim.this.minecraft.thePlayer.rotationYaw != -180.0F) && (AntiAim.this.minecraft.thePlayer.rotationPitch != 0.0F)) {
                    packet.setYaw(AntiAim.this.minecraft.thePlayer.rotationYaw);
                    packet.setPitch(AntiAim.this.minecraft.thePlayer.rotationPitch);
                }
            }
        }
    }

}
