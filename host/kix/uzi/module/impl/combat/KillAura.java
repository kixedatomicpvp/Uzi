package host.kix.uzi.module.impl.combat;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.module.value.Value;
import host.kix.uzi.utility.time.GameTimer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kix on 5/7/2017.
 */
@ModuleManifest(label = "KillAura", macro = 0, category = Category.COMBAT, color = 0x53DEB6)
public class KillAura extends Module {

    private Value<Boolean> teams = new Value<Boolean>("Teams", false);
    private Value<Integer> fov = new Value<Integer>("FOV", 360, 1, 360);
    private Value<Integer> delay = new Value<Integer>("Delay", 75, 0, 1000);
    private Value<Double> range = new Value<Double>("Range", 4.4, 3.0, 7.0);
    private Value<Integer> ticksExisted = new Value<Integer>("Ticks", 16, 0, 1000);
    private Value<Boolean> autoBlock = new Value<Boolean>("AutoBlock", true);
    private Value<Boolean> players = new Value<Boolean>("Players", true);
    private Value<Boolean> animals = new Value<Boolean>("Animals", false);
    private Value<Boolean> monsters = new Value<Boolean>("Monsters", false);
    private Value<Boolean> invisibles = new Value<Boolean>("Invisibles", false);
    private List<EntityLivingBase> loaded = new ArrayList<>();
    private EntityLivingBase target;
    private GameTimer time = new GameTimer();

    public KillAura() {
        setLabelWithSuffix("Kill Aura");
        getValues().add(teams);
        getValues().add(fov);
        getValues().add(delay);
        getValues().add(range);
        getValues().add(ticksExisted);
        getValues().add(autoBlock);
        getValues().add(players);
        getValues().add(animals);
        getValues().add(monsters);
        getValues().add(invisibles);
        target = null;
    }

    @Handler
    public void update(UpdateEvent event) {
        target = getBestEntity();
        boolean blockCheck = (autoBlock.getValue() && minecraft.thePlayer.inventory.getCurrentItem() != null && minecraft.thePlayer.inventory.getCurrentItem().getItem() != null && minecraft.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemSword);
        if (event.getType() == UpdateEvent.UpdateType.PRE && target != null) {
            event.setYaw(getRotations(target)[0]);
            event.setPitch(getRotations(target)[1]);
        } else {
            if (target != null && blockCheck && minecraft.thePlayer.getDistanceToEntity(target) < range.getValue())
                minecraft.playerController.sendUseItem(minecraft.thePlayer, minecraft.theWorld, minecraft.thePlayer.inventory.getCurrentItem());
            if (minecraft.thePlayer.isBlocking())
                minecraft.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, new BlockPos(0, 0, 0), EnumFacing.fromAngle(-255.0D)));
            if (target != null && time.reached(delay.getValue())) {
                attack(target);
                if (minecraft.getThePlayer().isBlocking())
                    minecraft.getThePlayer().sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(new BlockPos(0, 0, 0), 255, minecraft.getThePlayer().inventory.getCurrentItem(), 0.0F, 0.0F, 0.0F));
                target = null;
                time.reset();
            }
        }
    }

    public EntityLivingBase getBestEntity() {
        if (loaded != null) {
            loaded.clear();
        }
        for (Object object : minecraft.theWorld.loadedEntityList) {
            if (object instanceof EntityLivingBase) {
                EntityLivingBase e = (EntityLivingBase) object;
                if (isValid(e)) {
                    loaded.add(e);
                }
            }
        }
        if (loaded.isEmpty()) {
            return null;
        }
        loaded.sort((o1, o2) -> {
            float[] rot1 = getRotations(o1);
            float[] rot2 = getRotations(o2);
            return Float.compare((minecraft.thePlayer.rotationYaw - rot1[0]) % 0,
                    (minecraft.thePlayer.rotationYaw - rot2[0]) % 0);
        });
        return loaded.get(0);
    }


    public boolean isValid(EntityLivingBase entity) {
        if (teams.getValue() && entity != null) {
            final String name = entity.getDisplayName().getFormattedText();
            final StringBuilder append = new StringBuilder().append("§");
            if (name.startsWith(append.append(minecraft.getThePlayer().getDisplayName().getFormattedText().charAt(1)).toString())) {
                return false;
            }
        }
        return (entity != null
                && entity.isEntityAlive()
                && isEntityInFov(entity, fov.getValue())
                && entity != minecraft.getThePlayer()
                && (entity instanceof EntityPlayer
                && players.getValue()
                || entity instanceof EntityAnimal
                && animals.getValue()
                || entity instanceof EntityMob ||
                entity instanceof EntitySlime
                        && monsters.getValue())
                && entity.getDistanceToEntity(minecraft.getThePlayer()) <= range.getValue()
                && (!entity.isInvisible()
                || invisibles.getValue())
                && entity.ticksExisted > ticksExisted.getValue());
    }
}
