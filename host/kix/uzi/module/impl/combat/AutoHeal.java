package host.kix.uzi.module.impl.combat;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.input.PacketEvent;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.module.value.Value;
import host.kix.uzi.utility.time.GameTimer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemSoup;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.network.play.server.S2FPacketSetSlot;
import org.lwjgl.input.Keyboard;

/**
 * Created by Kix on 5/5/2017.
 */
@ModuleManifest(label = "AutoHeal", macro = Keyboard.KEY_P, color = 0xA8AFDE, category = Category.COMBAT)
public class AutoHeal extends Module {

    // Health
    private Value<Double> health = new Value<Double>("Health", 3D, 0D, 5D);
    // Delay
    private Value<Long> delay = new Value<Long>("Delay", 500L, 0L, 1000L);
    // Potions
    private Value<Boolean> potions = new Value<Boolean>("Potions", true);
    // Soup
    private Value<Boolean> soup = new Value<Boolean>("Soup", true);
    // Jump Pot
    private Value<Boolean> jumpPot = new Value<Boolean>("JumpPot", true);
    // Drop Soup
    private Value<Boolean> dropSoup = new Value<Boolean>("DropSoup", false);
    // The slot where the item will be used
    private int slot = -1;

    public AutoHeal() {
        setLabelWithSuffix("Auto Heal");
        getValues().add(health);
        getValues().add(delay);
        getValues().add(potions);
        getValues().add(soup);
        getValues().add(jumpPot);
        getValues().add(dropSoup);
    }

    /**
     * The packet slot being used to heal.
     */
    private int packetSlot = -1;

    /**
     * If the item used was a soup.
     */
    private boolean packetSoup = false;

    private ItemStack itemStack = null;

    // If the player is healing
    private boolean healing = false;
    private GameTimer gameTimer = new GameTimer();

    /**
     * completely ripped from graphite
     */
    @Handler
    public void update(UpdateEvent event) {
        if (event.getType() == UpdateEvent.UpdateType.PRE) {
            // Updates slot ifo and count.
            update();

            // Returns if the minecraft.thePlayer is fine.
            if (!shouldHeal()) return;

            // Return if slot is invalid.
            if (slot == -1) return;

            // If the minecraft.thePlayer should heal upwards.
            boolean up = jumpPot.getValue() && minecraft.thePlayer.onGround;

            // Does potion stuff if the slot has a potion in it.
            if (minecraft.thePlayer.inventory.mainInventory[slot].getItem() instanceof ItemPotion) {
                // Jumps if the potion should be thrown upwards.
                if (up) minecraft.thePlayer.jump();

                // Sets the pitch to up or down depending if the potion should go up or down.
                event.setPitch(up ? -90 : 90);
            }

            // Sets the state to healing.
            healing = true;
        } else {
            // Returns if not healing.
            if (!healing) return;

            // Returns if the minecraft.thePlayer is fine.
            if (!shouldHeal()) return;

            // Returns if slot is invalid.
            if (slot == -1) return;

            if (minecraft.thePlayer.inventory.mainInventory[8] != null)
                itemStack = minecraft.thePlayer.inventory.mainInventory[8].copy();

            // If the item was a soup.
            packetSoup = minecraft.thePlayer.inventory.mainInventory[slot].getItem() == Items.mushroom_stew;

            // Sets the packet slot to the current slot.
            packetSlot = slot;

            // If the slot is in the hotbar.
            if (slot < 9) {

                // Changes hotbar slot to the item.
                minecraft.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(slot));

                // Uses the item.
                minecraft.thePlayer.sendQueue.getNetworkManager().sendPacket(new C08PacketPlayerBlockPlacement(minecraft.thePlayer.getHeldItem()));

                // Drops the item.
                if (dropSoup.getValue() && packetSoup)
                    minecraft.thePlayer.dropItem(minecraft.thePlayer.inventory.mainInventory[slot].getItem(), 64);

                // Swaps back to the item being held.
                minecraft.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(minecraft.thePlayer.inventory.currentItem));
            } else {

                // Changes hotbar slot to the item.
                minecraft.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(8));

                // Moves the item.
                minecraft.playerController.windowClick(minecraft.thePlayer.inventoryContainer.windowId, slot, 8, 2, minecraft.thePlayer);

                // Uses the item.
                minecraft.getNetHandler().getNetworkManager().sendPacket(new C08PacketPlayerBlockPlacement(minecraft.thePlayer.getHeldItem()));

                // Drops the item.
                if (dropSoup.getValue() && packetSoup)
                    minecraft.thePlayer.dropItem(minecraft.thePlayer.inventory.mainInventory[slot].getItem(), 64);

                // Moves the item.
                minecraft.playerController.windowClick(minecraft.thePlayer.inventoryContainer.windowId, packetSlot, 8, 2, minecraft.thePlayer);

                // Swaps back to the item being held.
                minecraft.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(minecraft.thePlayer.inventory.currentItem));
            }

            // Resets the timer.
            gameTimer.reset();
        }
    }

    @Handler
    public void eventPacket(PacketEvent e) {
        /**
         * Fixed Autism by OG
         * */
        if (!(e.getType() == PacketEvent.PacketType.RECEIVE
                || packetSlot == -1
                || this.gameTimer.reached(1000)
        )) {
            minecraft.thePlayer.inventoryContainer.putStackInSlot(packetSlot, null);
        }
        if (!(!(e.getPacket() instanceof S2FPacketSetSlot)
                || e.getType() == PacketEvent.PacketType.RECEIVE
                || packetSlot == -1
                || gameTimer.reached(1000))) {

            S2FPacketSetSlot packet = (S2FPacketSetSlot) e.getPacket();

            if (packet.func_149173_d() == 44) {
                packet.setItem(itemStack);
            }
            packetSlot = -1;
        }
    }

    /**
     * Updates the count and finds an item to use.
     */
    public void update() {
        // The number of healing items in the inventory.
        int count = 0;

        // Sets the slot to -1 so nothing is used.
        slot = -1;

        // Iterates throw the inventory.
        for (int i = 0; i <= 35; i++) {

            // Continues threw list if item is air.
            if (minecraft.thePlayer.inventory.mainInventory[i] == null || minecraft.thePlayer.inventory.mainInventory[i].getItem() == null)
                continue;

            // The item stack in the slot.
            ItemStack stack = minecraft.thePlayer.inventory.mainInventory[i];

            // The type of the stack.
            Item item = stack.getItem();

            // If the item is a potion or souminecraft.thePlayer.
            if (item instanceof ItemPotion && potions.getValue()) {
                // Casts the item to a potion.
                ItemPotion potion = (ItemPotion) item;

                // Continues with loop if potion data isn't valid.
                if (stack.getMetadata() != 16421) continue;

                // Sets the current slot.
                slot = i;

                // Increases the count.
                count++;
            } else if (item instanceof ItemSoup && soup.getValue()) {
                // Sets the current slot.
                slot = i;

                // Increases the count.
                count++;
            }
        }
    }

    /**
     * @return If the player should be healed.
     */
    private boolean shouldHeal() {
        return gameTimer.reached(500) && minecraft.thePlayer.getHealth() <= 6;
    }

}
