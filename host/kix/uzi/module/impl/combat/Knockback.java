package host.kix.uzi.module.impl.combat;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.input.PacketEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import net.minecraft.network.play.server.S27PacketExplosion;

import java.awt.*;

/**
 * Created by Kix on 5/7/2017.
 */
@ModuleManifest(label = "Knockback", category = Category.COMBAT, color = 0xDE6D19)
public class Knockback extends Module {

    @Handler
    public void packet(PacketEvent event){
            if(event.getPacket() instanceof S12PacketEntityVelocity || event.getPacket() instanceof S27PacketExplosion){
                event.setCancelled(true);
            }
    }

}
