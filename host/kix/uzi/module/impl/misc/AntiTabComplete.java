package host.kix.uzi.module.impl.misc;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.input.PacketEvent;
import host.kix.uzi.events.update.TickEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.module.impl.misc.modes.antitabcomplete.Null;
import host.kix.uzi.module.impl.misc.modes.antitabcomplete.Spoof;
import net.minecraft.network.play.client.C14PacketTabComplete;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.Random;

/**
 * Created by Kix on 5/6/2017.
 */
@ModuleManifest(label = "AntiTabComplete", macro = Keyboard.KEY_Z, category = Category.MISC, color = 0x9FDE74)
public class AntiTabComplete extends Module {

    public AntiTabComplete() {
        setLabelWithSuffix("Anti Tab Complete");
        addMode(new Spoof(this));
        addMode(new Null(this));
    }

}
