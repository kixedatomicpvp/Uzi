package host.kix.uzi.module.impl.misc;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.input.PacketEvent;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.utility.time.GameTimer;
import net.minecraft.network.Packet;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Kix on 5/12/2017.
 */
@ModuleManifest(label = "FakeLag", category = Category.MISC, color = 0xFF0000)
public class FakeLag extends Module {

    private final List<Packet> packets = new CopyOnWriteArrayList<>();
    private final GameTimer timer = new GameTimer();


    @Handler
    public void update(UpdateEvent event) {
        if (timer.reached(1000L)) {
            for (Packet packet : packets) {
                minecraft.getNetHandler().getNetworkManager().sendPacket(packet);
                packets.remove(packet);
            }
            timer.reset();
        }
    }

    @Handler
    public void sendPacket(PacketEvent event) {
        if (!minecraft.thePlayer.isDead) {
            if (!event.isCancelled()) {
                packets.add(event.getPacket());
                event.setCancelled(true);
            }
        }

    }

    @Override
    public void onDisable() {
        super.onDisable();
        for (Packet packet : packets) {
            minecraft.getNetHandler().addToSendQueue(packet);
            packets.remove(packet);
        }
    }
}
