package host.kix.uzi.module.impl.misc.modes.antitabcomplete;

import host.kix.uzi.module.framework.ModuleMode;
import host.kix.uzi.module.impl.misc.AntiTabComplete;
import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.input.PacketEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.network.play.client.C14PacketTabComplete;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.Random;

/**
 * Created by Kix on 5/7/2017.
 */
public class Null extends ModuleMode<AntiTabComplete> {

    public Null(AntiTabComplete parent) {
        super(parent, "Null");
    }

    @Handler
    public void packet(PacketEvent event) {
        if (event.getType() == PacketEvent.PacketType.SEND) {
            if (event.getPacket() instanceof C14PacketTabComplete) {
                C14PacketTabComplete packet = (C14PacketTabComplete) event.getPacket();
                if (packet.getMessage().startsWith(".")) {
                    event.setCancelled(true);
                }
            }

        }
    }

}
