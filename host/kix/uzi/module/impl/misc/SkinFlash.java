package host.kix.uzi.module.impl.misc;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.utility.time.GameTimer;
import net.minecraft.entity.player.EnumPlayerModelParts;

import java.util.Random;

/**
 * Created by Kix on 5/13/2017.
 */
@ModuleManifest(label = "SkinFlash", category = Category.MISC, color = 0xF57398)
public class SkinFlash extends Module {

    private final Random random = new Random();
    private GameTimer timer = new GameTimer();

    @Handler
    public void updateGameLoop(UpdateEvent event) {
        EnumPlayerModelParts[] parts = EnumPlayerModelParts.values();
        for (EnumPlayerModelParts part : parts) {
            if(timer.reached(100)) {
                minecraft.gameSettings.func_178878_a(part, random.nextBoolean());
                timer.reset();
            }
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();
        if (this.minecraft.thePlayer == null) {
            return;
        }
        EnumPlayerModelParts[] parts = EnumPlayerModelParts.values();
        if (parts != null) {
            for (EnumPlayerModelParts part : parts) {
                this.minecraft.gameSettings.func_178878_a(part, true);
            }
        }
    }
}
