package host.kix.uzi.module.impl.misc;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.network.play.client.C0APacketAnimation;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Kix on 5/5/2017.
 */
@ModuleManifest(label = "Retard", macro = Keyboard.KEY_N, category = Category.MISC, color = 0xDE9AD2)
public class Retard extends Module {

    private float spin;

    @Handler
    public void onUpdate(UpdateEvent eventUpdate) {
        spin += 20;
        if (spin > 180) {
            spin = -180;
        } else if (spin < -180) {
            spin = 180;
        }
        eventUpdate.setYaw(spin);
        eventUpdate.setPitch(-180);
        minecraft.getNetHandler().addToSendQueue(new C0APacketAnimation());
    }


}
