package host.kix.uzi.module.impl.misc;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.input.PacketEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;

/**
 * Created by Kix on 5/12/2017.
 */
@ModuleManifest(label="NoRotate", category = Category.MISC, color = 0x38A3A8)
public class NoRotate extends Module {

    @Handler
    public void readPacketData(PacketEvent event){
        if(event.getType() == PacketEvent.PacketType.RECEIVE){
            if (minecraft.thePlayer == null || minecraft.theWorld == null)
                return;
            if (event.getPacket() instanceof S08PacketPlayerPosLook) {
                S08PacketPlayerPosLook poslook = (S08PacketPlayerPosLook) event.getPacket();
                if (minecraft.thePlayer.rotationYaw != -180 && minecraft.thePlayer.rotationPitch != 0) {
                    poslook.yaw = minecraft.thePlayer.rotationYaw;
                    poslook.pitch = minecraft.thePlayer.rotationPitch;
                }
            }

        }
    }

}
