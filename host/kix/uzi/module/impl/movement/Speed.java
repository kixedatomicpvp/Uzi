package host.kix.uzi.module.impl.movement;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockLiquid;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.potion.Potion;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Kix on 5/5/2017.
 */
@ModuleManifest(label = "Speed", macro = Keyboard.KEY_V, category = Category.MOVEMENT, color = 0xDEDA73)
public class Speed extends Module {

    @Override
    public void onDisable() {
        minecraft.getTimer().timerSpeed = 1f;
        super.onDisable();
    }

    @Handler
    public void update(UpdateEvent event) {
        if (event.type == UpdateEvent.UpdateType.PRE) {
            if (!(this.isOnLiquid() || this.isInLiquid())) {
                if (isMoving(this.minecraft.thePlayer) && this.minecraft.thePlayer.onGround) {
                    if (this.minecraft.thePlayer.ticksExisted % 2 != 0 && !this.minecraft.isSingleplayer()
                            && !this.minecraft.thePlayer.movementInput.jump) {
                        event.y = this.minecraft.thePlayer.posY + 0.4;
                        event.onGround = false;
                    }
                    float speed = this.minecraft.thePlayer.ticksExisted % 2 == 0 ? 0.15f * 2.32F : 0.15F;
                    this.minecraft.thePlayer.motionX = -(Math.sin(this.getDirection()) * speed);
                    this.minecraft.thePlayer.motionZ = Math.cos(this.getDirection()) * speed;

                    minecraft.getTimer().timerSpeed = 1.095f;
                }
            }
        }

    }

    public float getDirection() {
        float yaw = minecraft.thePlayer.rotationYaw;
        if (minecraft.thePlayer.moveForward < 0) {
            yaw += 180;
        }
        float forward = 1;
        if (minecraft.thePlayer.moveForward < 0) {
            forward = -0.5F;
        } else if (minecraft.thePlayer.moveForward > 0) {
            forward = 0.5F;
        }
        if (minecraft.thePlayer.moveStrafing > 0) {
            yaw -= 90 * forward;
        }
        if (minecraft.thePlayer.moveStrafing < 0) {
            yaw += 90 * forward;
        }
        yaw *= 0.017453292F;
        return yaw;
    }

    private double getBaseMoveSpeed() {
        double baseSpeed = 0.2873;
        if (minecraft.thePlayer.isPotionActive(Potion.moveSpeed)) {
            int amplifier = minecraft.thePlayer.getActivePotionEffect(Potion.moveSpeed).getAmplifier();
            baseSpeed *= 1 + 0.2 * (amplifier + 1);
        }
        return baseSpeed;
    }

    public float getSpeed() {
        return (float) Math.sqrt((minecraft.thePlayer.motionX * minecraft.thePlayer.motionX)
                + (minecraft.thePlayer.motionZ * minecraft.thePlayer.motionZ));
    }


    public boolean isInLiquid() {
        if (minecraft.thePlayer == null)
            return false;
        boolean inLiquid = false;
        int y = (int) minecraft.thePlayer.boundingBox.minY;
        for (int x = MathHelper.floor_double(minecraft.thePlayer.boundingBox.minX); x < MathHelper
                .floor_double(minecraft.thePlayer.boundingBox.maxX) + 1; x++) {
            for (int z = MathHelper.floor_double(minecraft.thePlayer.boundingBox.minZ); z < MathHelper
                    .floor_double(minecraft.thePlayer.boundingBox.maxZ) + 1; z++) {
                Block block = minecraft.theWorld.getBlockState(new BlockPos(x, y, z))
                        .getBlock();
                if ((block != null) && (!(block instanceof BlockAir))) {
                    if (!(block instanceof BlockLiquid))
                        return false;
                    inLiquid = true;
                }
            }
        }
        return inLiquid;
    }

    public boolean isOnLiquid() {
        if (minecraft.thePlayer == null)
            return false;
        boolean onLiquid = false;
        int y = (int) minecraft.thePlayer.boundingBox.offset(0.0D, -0.01D, 0.0D).minY;
        for (int x = MathHelper.floor_double(minecraft.thePlayer.boundingBox.minX); x < MathHelper
                .floor_double(minecraft.thePlayer.boundingBox.maxX) + 1; x++) {
            for (int z = MathHelper.floor_double(minecraft.thePlayer.boundingBox.minZ); z < MathHelper
                    .floor_double(minecraft.thePlayer.boundingBox.maxZ) + 1; z++) {
                Block block = minecraft.theWorld.getBlockState(new BlockPos(x, y, z))
                        .getBlock();
                if ((block != null) && (!(block instanceof BlockAir))) {
                    if (!(block instanceof BlockLiquid))
                        return false;
                    onLiquid = true;
                }
            }
        }
        return onLiquid;
    }

    public static boolean isMoving(Entity ent) {
        return ent == Minecraft.getMinecraft().thePlayer
                ? ((Minecraft.getMinecraft().thePlayer.moveForward != 0
                || Minecraft.getMinecraft().thePlayer.moveStrafing != 0))
                : (ent.motionX != 0 || ent.motionZ != 0);
    }


}
