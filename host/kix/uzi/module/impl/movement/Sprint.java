package host.kix.uzi.module.impl.movement;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.TickEvent;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Kix on 5/5/2017.
 */
@ModuleManifest(label = "Sprint", macro = Keyboard.KEY_Y, category = Category.MOVEMENT, color = 0xDE828C)
public class Sprint extends Module {

    @Handler
    public void tick(UpdateEvent event){
        if(!minecraft.thePlayer.isSprinting() && minecraft.thePlayer.isEntityAlive()) {
            minecraft.thePlayer.setSprinting(canSprint());
        }
    }

    /**
     * @return whether or not the player is able to sprint
     */
    private boolean canSprint() {
        return !minecraft.thePlayer.isCollidedHorizontally && !minecraft.thePlayer.isSneaking()
                && minecraft.thePlayer.getFoodStats().getFoodLevel() > 5
                && (minecraft.thePlayer.movementInput.moveForward != 0.0f || minecraft.thePlayer.movementInput.moveStrafe != 0.0f);

    }

}
