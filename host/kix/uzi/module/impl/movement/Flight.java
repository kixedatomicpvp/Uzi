package host.kix.uzi.module.impl.movement;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;

/**
 * Created by Kix on 5/13/2017.
 */
@ModuleManifest(label = "Flight", category = Category.MOVEMENT, color = 0xF59373)
public class Flight extends Module {

    @Handler
    public void updateGameLoop(UpdateEvent event){
        if(!minecraft.thePlayer.capabilities.isFlying)
        minecraft.thePlayer.capabilities.isFlying = true;
    }

    @Override
    public void onDisable() {
        super.onDisable();
        minecraft.thePlayer.capabilities.isFlying = false;
    }
}
