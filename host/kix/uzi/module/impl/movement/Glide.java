package host.kix.uzi.module.impl.movement;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;

/**
 * Created by Kix on 5/13/2017.
 */
@ModuleManifest(label = "Glide", category = Category.MOVEMENT, color = 0x73C1F5)
public class Glide extends Module {

    @Handler
    public void updateGameLoop(UpdateEvent event) {
        minecraft.thePlayer.motionY = -0.05D;
        if (minecraft.gameSettings.keyBindJump.isPressed()) {
            minecraft.thePlayer.motionY = 0.05D;
        }
    }

}
