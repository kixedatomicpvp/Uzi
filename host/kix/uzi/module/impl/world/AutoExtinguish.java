package host.kix.uzi.module.impl.world;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * Created by Kix on 5/13/2017.
 * Automatically extinguishes the player if they are on fire. Serves as a fire alarm almost
 */
@ModuleManifest(label = "AutoExtinguish", category = Category.WORLD, color = 0x73F5EE)
public class AutoExtinguish extends Module {

    /**
     * Handles the game loop
     * @param event
     */
    @Handler
    public void updateGameLoop(UpdateEvent event){
        if(minecraft.getThePlayer().isBurning() && this.isRunning()) {
            for (int i = 0; i <= 60; i++) {
                minecraft.getThePlayer().sendQueue.addToSendQueue(new C03PacketPlayer(true));
            }
        }
    }

}
