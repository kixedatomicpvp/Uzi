package host.kix.uzi.module.impl.world;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;

/**
 * Created by Kix on 5/13/2017.
 */
@ModuleManifest(label = "AutoRespawn", category = Category.WORLD, color = 0xFFD900)
public class AutoRespawn extends Module {

    @Handler
    public void update(UpdateEvent event){
        if(minecraft.thePlayer.isDead){
            minecraft.thePlayer.respawnPlayer();
        }
    }

}
