package host.kix.uzi.module.impl.world;

import com.aeonicdev.seraphim.api.eventing.listen.Handler;
import host.kix.uzi.events.update.UpdateEvent;
import host.kix.uzi.module.framework.Category;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;

/**
 * Created by Kix on 5/13/2017.
 */
@ModuleManifest(label = "FastPlace", category = Category.WORLD, color = 0x60C42D)
public class FastPlace extends Module {

    @Handler
    public void updateGameLoop(UpdateEvent event){
        if(this.isRunning()){
            minecraft.setRightClickDelayTimer(0);
        }else{
            minecraft.setRightClickDelayTimer(4);
        }
    }

}
