package host.kix.uzi.command.impl;

import host.kix.uzi.command.framework.Command;
import host.kix.uzi.command.framework.annotations.CommandManifest;
import net.minecraft.client.Minecraft;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * Created by Kix on 5/12/2017.
 */
@CommandManifest(label="GrabIp", description = "Grabs the server IP", handles = {"grab", "ip"})
public class GrabIPCommand extends Command{

    @Override
    public void init(String message) {
        String ip = Minecraft.getMinecraft().getCurrentServerData().serverIP;
        StringSelection ipStringSelection = new StringSelection(ip);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(ipStringSelection, null);
    }
}
