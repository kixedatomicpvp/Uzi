package host.kix.uzi.command.impl;

import host.kix.uzi.Uzi;
import host.kix.uzi.command.framework.Command;
import host.kix.uzi.command.framework.annotations.CommandManifest;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.utility.chat.Logger;
import org.lwjgl.input.Keyboard;

/**
 * Created by Kix on 5/12/2017.
 */
@CommandManifest(label="Bind", description = "Allows the user to change binds for modules", handles = {"bind", "b", "macro"})
public class BindCommand extends Command {

    @Override
    public void init(String message) {
        String[] args = message.split(" ");
        Module module = Uzi.getUzi().getModuleManager().find(args[1]);
        int macro = Keyboard.getKeyIndex(args[2].toUpperCase());
        if(module != null){
            module.setMacro(macro);
            Logger.logToChat(module.getLabel() + " has been bound to " + macro);
        }else{
            Logger.logToChat("Module not found!");
        }
    }
}
