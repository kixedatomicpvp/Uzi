package host.kix.uzi.command.impl;

import host.kix.uzi.command.framework.Command;
import host.kix.uzi.command.framework.annotations.CommandManifest;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * Created by Kix on 5/12/2017.
 */
@CommandManifest(label = "Lenny", description = "Copies the lenny emoji", handles = {"lennyface", "len"})
public class LennyCommand extends Command {

    @Override
    public void init(String message) {
        String lennyFace = "( ͡° ͜ʖ ͡°)";
        StringSelection lennyFaceStringSelection = new StringSelection(lennyFace);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(lennyFaceStringSelection, null);
    }
}
