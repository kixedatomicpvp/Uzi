package host.kix.uzi.command.impl;

import host.kix.uzi.Uzi;
import host.kix.uzi.command.framework.Command;
import host.kix.uzi.command.framework.annotations.CommandManifest;
import host.kix.uzi.utility.chat.Logger;

/**
 * Created by Kix on 5/6/2017.
 */
@CommandManifest(label = "Help", description = "Gives Help to the User", handles = {"h", "he", "hel", "help", "helpme", "commands", "modules"})
public class HelpCommand extends Command {

    @Override
    public void init(String message) {
        String[] args = message.split(" ");
        if (args.length > 1) {
            if (args[1].equalsIgnoreCase("mods") || args[1].equalsIgnoreCase("modules")) {
                StringBuilder mods = new StringBuilder("Modules (" + Uzi.getUzi().getModuleManager().getContents().size() + "): ");
                Uzi.getUzi().getModuleManager().getContents().stream().forEach(mod -> mods.append(mod.isRunning() ? "\247a" : "\247c").append(mod.getLabel()).append("\247r, "));
                Logger.logToChat(mods.toString().substring(0, mods.length() - 2));
            }
            Uzi.getUzi().getModuleManager().getContents().forEach(module -> {
                if (args[1].equalsIgnoreCase(module.getLabel())) {
                    if (!module.getValues().isEmpty()) {
                        StringBuilder values = new StringBuilder("Values (" + module.getValues().size() + "): ");
                        module.getValues().stream().forEach(value -> values.append(value.getName()).append("\2477, \2477"));
                        Logger.logToChat(values.toString().substring(0, values.length() - 2));
                    }
                }
            });
        } else {
            Uzi.getUzi().getCommandManager().getContents().forEach(command -> {
                Logger.logToChat(command.getLabel() + " \2478: " + "\2477" + command.getDescription() + "\2477");
            });
        }
    }
}
