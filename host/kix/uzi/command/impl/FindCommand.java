package host.kix.uzi.command.impl;

import host.kix.uzi.Uzi;
import host.kix.uzi.command.framework.Command;
import host.kix.uzi.command.framework.annotations.CommandManifest;
import host.kix.uzi.module.framework.Module;
import host.kix.uzi.module.framework.annotations.ModuleManifest;
import host.kix.uzi.utility.chat.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Kix on 5/13/2017.
 */
@CommandManifest(label = "Find", description = "Helps the user find modules", handles = {"f", "search"})
public class FindCommand extends Command {
    @Override
    public void init(String message) {
        if (message.split(" ").length > 1) {
            Uzi.getUzi().getModuleManager().getContents().forEach(module -> {
                if (module.getLabel().contains(message.split(" ")[1])) {
                    Logger.logToChat(module.getLabel());
                }
            });
        } else {
            Logger.logToChat("Please add a letter or word in the first argument");
        }
    }
}
