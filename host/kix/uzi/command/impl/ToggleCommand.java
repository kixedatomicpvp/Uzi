package host.kix.uzi.command.impl;

import host.kix.uzi.Uzi;
import host.kix.uzi.command.framework.Command;
import host.kix.uzi.command.framework.annotations.CommandManifest;
import host.kix.uzi.module.framework.Module;

/**
 * Created by Kix on 5/7/2017.
 */
@CommandManifest(label = "Toggle", description = "Allows the user to toggle modules.", handles = {"t", "turnon", "on", "setEnabled", "setState", "setRunning", "turnonlikeahooker"})
public class ToggleCommand extends Command {

    @Override
    public void init(String message) {
        String[] args = message.split(" ");
        Module module = Uzi.getUzi().getModuleManager().find(args[1]);
        if(module != null){
            module.toggle();
        }
    }
}
