package host.kix.uzi.command.impl;

import host.kix.uzi.command.framework.Command;
import host.kix.uzi.command.framework.annotations.CommandManifest;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * Created by Kix on 5/12/2017.
 */
@CommandManifest(label = "TableFlip", description = "Copies the table flip emoji", handles = {"tf", "table"})
public class TableFlipCommand extends Command {

    @Override
    public void init(String message) {
        String tableFlip = "(╯°□°）╯︵ ┻━┻";
        StringSelection tableFlipStringSelection = new StringSelection(tableFlip);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(tableFlipStringSelection, null);
    }
}
