package host.kix.uzi.command.framework;

import host.kix.uzi.command.framework.annotations.CommandManifest;

/**
 * Created by Kix on 5/6/2017.
 */
public class Command {

    /**
     * Labels the command
     */
    private String label;

    /**
     * Describes the command
     */
    private String description;

    /**
     * Allows the use of secondary aliases for the command
     */
    private String[] handles;


    public Command(String label, String description, String... handles) {
        this.label = label;
        this.description = description;
        this.handles = handles;
    }

    public Command() {
        CommandManifest manifesto = this.getClass().getAnnotation(CommandManifest.class);
        label = manifesto.label();
        description = manifesto.description();
        handles = manifesto.handles();
    }

    public String getLabel() {
        return label;
    }

    public void init(String message) {

    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getHandles() {
        return handles;
    }

    public void setHandles(String[] handles) {
        this.handles = handles;
    }
}
