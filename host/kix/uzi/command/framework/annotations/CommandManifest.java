package host.kix.uzi.command.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Kix on 5/6/2017.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface CommandManifest {

    /**
     * Labels the command
     */
    String label();

    /**
     * Describes the command for the user
     */
    String description();

    /**
     * Gives the possible handles for the command
     */
    String[] handles();

}
