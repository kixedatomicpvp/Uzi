package host.kix.uzi.utility;

import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.util.MathHelper;

/**
 * Created by Kix on 5/5/2017.
 */
public class ModuleHelper {

    protected Minecraft minecraft = Minecraft.getMinecraft();

    protected boolean isEntityInFov(final EntityLivingBase entity, double angle) {
        angle *= 0.5;
        final double angleDifference = getAngleDifference(minecraft.thePlayer.rotationYaw, getRotations(entity)[0]);
        return (angleDifference > 0.0 && angleDifference < angle)
                || (-angle < angleDifference && angleDifference < 0.0);
    }

    public void attack(EntityLivingBase entity) {
        final float sharpLevel = EnchantmentHelper.func_152377_a(minecraft.getThePlayer().getHeldItem(), minecraft.getThePlayer().getCreatureAttribute());
        if(sharpLevel > 0){
            minecraft.getThePlayer().onEnchantmentCritical(entity);
        }
        minecraft.getThePlayer().swingItem();
        minecraft.getThePlayer().sendQueue.addToSendQueue(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));
    }

    protected float[] getRotations(Entity entity) {
        if (entity == null)
            return null;

        double diffX = entity.posX - minecraft.getThePlayer().posX;
        double diffZ = entity.posZ - minecraft.getThePlayer().posZ;
        double diffY;
        if ((entity instanceof EntityLivingBase)) {
            EntityLivingBase elb = (EntityLivingBase) entity;
            diffY = elb.posY
                    + (elb.getEyeHeight() - 0.4)
                    - (minecraft.getThePlayer().posY + minecraft.getThePlayer()
                    .getEyeHeight());
        } else {
            diffY = (entity.boundingBox.minY + entity.boundingBox.maxY)
                    / 2.0D
                    - (minecraft.getThePlayer().posY + minecraft.getThePlayer()
                    .getEyeHeight());
        }
        double dist = MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
        float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) -(Math.atan2(diffY, dist) * 180.0D / Math.PI);

        return new float[]{yaw, pitch};
    }

    protected float getAngleDifference(float direction, float rotationYaw) {
        float phi = Math.abs(rotationYaw - direction) % 360;
        float distance = phi > 180 ? 360 - phi : phi;
        return distance;
    }

}
