package host.kix.uzi.utility.chat;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;

/**
 * Created by Kix on 5/6/2017.
 */
public class Logger {

    private static String prefix = "\2478[\2477Uzi\2478] \2477";

    public static void logToChat(String message){
        Minecraft.getMinecraft().thePlayer.addChatComponentMessage(new ChatComponentText(prefix + message));
    }

}
