package host.kix.uzi.utility.time;

/**
 * Created by Kix on 5/5/2017.
 */
public class GameTimer {

    /**
     * The time when the timer was created.
     */
    private long now = System.currentTimeMillis();

    /**
     * If the timer has passed the delay.
     *
     * @param delay delay to be checked.
     * @return if the delay has been reached.
     */
    public boolean reached(long delay){
        return System.currentTimeMillis() - now >= delay;
    }

    /**
     * Resets the timer.
     */
    public void reset(){
        now = System.currentTimeMillis();
    }

    /**
     * Returns the time passed since reset or creation of the timer.
     *
     * @return time passed.
     */
    public long getTimePassed(){
        return System.currentTimeMillis() - now;
    }

}
